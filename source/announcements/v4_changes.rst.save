.. _v4-changes:

Changes in PFLOTRAN 4.0
-----------------------
The following has changed in PFLOTRAN 4.0:

 * For RICHARDS and TH, the following keywords changed to align with the
   other flow modes. Although 4.0 is still backwards compatible, the old 
   keywords will be deprecated after the release of 4.0.

  * PRESSURE -> LIQUID_PRESSURE
  * SATURATION -> LIQUID_SATURATION
  * FLUX -> LIQUID_FLUX

 * *\\-\\-download-hdf5-fortran-bindings=yes* is now required in the configuration
   of PETSc (3.16+) to force the building of the HDF5 modules (.mod) for
   use within the *use statements* in PFLOTRAN.

 * Instructions for building PFLOTRAN on the Mac and Windows (using Cygwin) 
   have been removed. Use the Linux instructions for the Mac.

 * Instructions for building the legacy (non-process model) version of 
   PFLOTRAN have been removed.

 * The :ref:`developer-guide` has been updated to include the methodology, life
   cycle and a configuration management plan.

