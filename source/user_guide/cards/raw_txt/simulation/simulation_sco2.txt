ISOTHERMAL_TEMPERATURE <float>
  Sets an isothermal temperature for the simulation. The energy balance is not used if this is invoked.