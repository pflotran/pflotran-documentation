Back to :ref:`card-index`

.. _survey-card:

SURVEY
======
Specifies information about the ERT survey file.

Required Cards:
---------------
FILE_NAME <string>
 Specifies the name of the survey file.

Optional Cards:
---------------
FORMAT <string>
 Specifies the format of the survey file.

Examples
--------

::

 SURVEY
   FILE_NAME ert.srv
   FORMAT E4D_SRV
 END

