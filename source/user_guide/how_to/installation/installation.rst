.. _installation:

*******************
Installing PFLOTRAN
*******************

**PFLOTRAN is on** `Bitbucket`_ 

PFLOTRAN can be installed on Linux, Mac, and Windows systems, or through a
virtual machine. 
We provide instructions for these systems in the links below. 
Please note that a Windows installer is not available.
The use of Windows Subsystem for Linux is highly recommended for Windows
as compiling through Cygwin and Visual Studio is very difficult and
prone to error.

* :ref:`linux-install`

* :ref:`windows-subsystem-for-linux-install`

* :ref:`windows-visual-studio-install`

* :ref:`previous-petsc-releases`

* :ref:`machine-specific-petsc-configs`

* :ref:`linux-vm`

.. _Bitbucket: https://bitbucket.org/pflotran/pflotran/wiki/Home


 
