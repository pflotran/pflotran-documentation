.. PFLOTRAN documentation master file, created by Glenn Hammond on 2019-03-18

.. raw:: html
  :file: google_analytics.html
  
**********************
PFLOTRAN Documentation
**********************
.. image:: /_static/pflotran_logo.jpg

**This is the development version of the documentation.**

PFLOTRAN is an open source, state-of-the-art massively parallel subsurface flow 
and reactive transport code. The code is developed under a GNU LGPL license 
allowing for third parties to interface proprietary software with the code, 
however any modifications to the code itself must be documented and remain open 
source. PFLOTRAN is written in object oriented, free formatted Fortran 2003. 
The choice of Fortran over C/C++ was based primarily on the need to enlist and 
preserve tight collaboration with experienced domain scientists, without which 
PFLOTRAN's sophisticated process models would not exist. PFLOTRAN's source code repository is on Bitbucket_.

Announcements
=============

.. toctree::
   :maxdepth: 1

..
   announcements/v4_changes.rst

Documentation
=============

.. toctree::
   :maxdepth: 1

   developer_guide/developer_guide.rst
   theory_guide/theory_guide.rst
   user_guide/user_guide.rst

--------------------------------

Need help beyond the documentation above?
 | Search topics at or submit a question to the `pflotran-users`_ Google Group.
 | Email user questions to pflotran-users at googlegroups dot com
 | Email bug reports to pflotran-dev at googlegroups dot com

.. _Bitbucket: https://bitbucket.org/pflotran/pflotran/wiki/Home
.. _pflotran-users: https://groups.google.com/d/forum/pflotran-users

