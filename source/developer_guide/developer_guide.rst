.. _developer-guide:

Developer Guide
===============

This PFLOTRAN Developer Guide better ensures PFLOTRAN quality, integrity, robustness and longevity (for the PFLOTRAN community) by training code developers, who will be making modifications to documentation, source code or tests, regarding effective software development practices.  A key requirement for receiving permission to contribute to the online PFLOTRAN Bitbucket repository (aka push privileges) is to review this document and discuss the guidelines with a Senior PFLOTRAN Developer. Developers receive additional training through the review and approval process that is required to merge new features to the repository.

.. toctree::
   :maxdepth: 2
   
   /developer_guide/developer_guide_glossary.rst
   /developer_guide/development_methodology.rst
   /developer_guide/development_life_cycle.rst
   /developer_guide/configuration_management_plan.rst
   /developer_guide/software_productivity_sustainability.rst
   /developer_guide/review_and_approval.rst
   /developer_guide/maintenance_and_corrective_action.rst

**Standards:**

.. toctree::
   :maxdepth: 2
   
   /developer_guide/contributing_to_pflotran.rst
   /developer_guide/training/fortran_coding_standard.rst

QA Test Suite
-------------

.. toctree::
   :maxdepth: 2
   
   /qa_tests/pc_sat_rel_perm.rst

..
   /qa_tests/intro_howto.rst
   /qa_tests/intro_thermal.rst
   /qa_tests/intro_flow.rst
   /qa_tests/intro_gas.rst
   /qa_tests/intro_transport.rst

..
  .. toctree::
     :hidden:
     
     include_toctree_thermal_steady.rst
     include_toctree_thermal_transient.rst
     include_toctree_flow_steady.rst
     include_toctree_flow_transient.rst
     include_toctree_gas_steady.rst
     include_toctree_transport_steady.rst
     include_toctree_transport_transient.rst

FAQ
---

.. toctree::
   :maxdepth: 2
   
   /user_guide/how_to/faq.rst
   
   
   
   
   
.. comment
   Indices and tables
   ==================
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

