Back to :ref:`developer-guide`

.. _developer-guide-glossary:

Glossary
========
- **Bitbucket:** a cloud-based version control repository that employs Git version control.
- **Codecov:** a configurable continuous integration service hosted in the cloud that leverages the GNU gcov and lcov tools to document the lines of code covered by a suite of tests.
- **Codeship:** a configurable continuous integration service hosted in the cloud for automatically building and testing software.
- **Contributor:** a code developer who adds capability to PFLOTRAN through pull requests from forked repositories.  A contributor lacks push privileges to the main repository.
- **Developer:** a code developer familiar with PFLOTRAN and capable of modifying the source code to add features, eliminate bugs, or streamline performance.
- **Feature branch:** a branch of the version control system that is not master.
- **Git:** a distributed version control system for tracking changes to ASCII text and binary files.
- **Google Groups:** an online mailing list where users may subscribe and participate in group discussions either by email or access to Google Groups online. In the case of pflotran-users and pflotran-dev, anyone may post a message to the mailing list, and members receive emails, either by individual email or a digest.
- **Jira:** project management software that tracks tasks in prescribed workflows from beginning to end. Administrators assign and monitor tasks while users can document progress and update task status in the workflow.
- **Lead Developer:** The single code developer who is the principal developer of PFLOTRAN. 
- **Master branch:** the main development branch of the version control that is modified through the merging of feature branches. A specific commit within the master branch is tagged during software release.
- **Maintenance branch:** a frozen branch that originates from a (tagged commit in the) master branch during release. Only bug fixes and minor corrective actions are performed in these branches.
- **pflotran.org:** PFLOTRAN’s main website.
- **pull request:** a formal process for submitting a set of changes to a code development project.
- **reStructuredText:** a lightweight markup language for technical documentation.
- **Semantic versioning:** a versioning system that utilizes the format MAJOR.MINOR.PATCH, where MINOR versions must support backwards compatibility within MAJOR versions while MAJOR version need not.  PATCH versions are released for maintenance updates (e.g. bug fixes, minor corrections).
- **Senior Developer:** a code developer with push privileges to the maintenance and master branches in the main project repository and merging responsibility.
- **Sphinx:** a Python-based documentation generator that converts reStructuredText to HTML, LaTeX (for PDF generation), ePub, TextInfo, manual pages or plain text output.
- **Third-party libraries:** Software libraries developed and maintained by external entities that are linked to the PFLOTRAN source code during compilation.
- **Travis-CI:** a configurable continuous integration service hosted in the cloud for automatically building and testing software.
- **User:** a person who utilizes PFLOTRAN to simulate Earth system processes.
- **Version control:** a system that records changes to a file or set of files over time so that one can recall specific versions later.
