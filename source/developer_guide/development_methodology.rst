Back to :ref:`developer-guide`

.. _development-methodology:

Development Methodology
=======================

To the extent possible, PFLOTRAN employs agile software development.  
`The Manifesto for Agile Software Development <http://agilemanifesto.org>`_ states:

We are uncovering better ways of developing software by doing it and helping others do it. Through this work we have come to value:

- **Individuals and interactions** over processes and tools
- **Working software** over comprehensive documentation
- **Customer collaboration** over contract negotiation
- **Responding to change** over following a plan

That is, while there is value in the items on the right, we value the items on the left more.

It is understood that individual projects developing PFLOTRAN may value 
(or require) items on the right more than on the left to meet software 
quality requirements.  But the items on the left are still highly valued
and play important roles in the PFLOTRAN developer community.  The 
objective of PFLOTRAN’s agile methodology is to continually develop and 
evolve PFLOTRAN in support of its open source community needs.  To this 
end, PFLOTRAN embraces tools such as distributed version control, 
cloud-based repository hosting, and continuous integration services to 
preserve software integrity.
