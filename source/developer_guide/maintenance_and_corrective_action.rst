Back to :ref:`developer-guide`

.. _maintenance_and_corrective_action:

Maintenance, Problem Reporting and Corrective Action
====================================================
Users may request new features or report issues through the 
developer or user mailing lists.  Senior Developers review these 
emails.  They document feature requests in Jira.  Based upon 
availability, funding, and interest, Senior Developers assign 
the feature request to a competent developer who can support the 
request.  Issues may include inaccurate documentation, incorrect 
implementation, user error, bugs, etc. Issues are verified and/or 
replicated to rule out user error.  As with feature request, 
Senior Developers assign confirmed issues to Developers capable 
of correcting the issue.  In all cases, Developers notify the 
reporting party regarding any corrective action.  Note that 
Developers with access to Jira may submit feature requests and 
issues directly.
