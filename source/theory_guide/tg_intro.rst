Introduction to the Governing Equations
=======================================

* :ref:`symbol-glossary`

* :ref:`mode-richards`

* :ref:`mode-th`

* :ref:`mode-general`

* :ref:`mode-hydrate`

* :ref:`mode-mphase`

* :ref:`mode-immis`

* :ref:`mode-miscible`

* :ref:`mode-sco2`

* :ref:`mode-reactive-transport`

* :ref:`mode-geomechanics`

* :ref:`multiple_continuum`