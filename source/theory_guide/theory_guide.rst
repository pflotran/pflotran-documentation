.. _theory-guide:

Theory Guide
============

.. toctree::
   :maxdepth: 1

   /theory_guide/symbol_glossary.rst


Flow Modes
++++++++++

.. toctree::
   :maxdepth: 2

   /theory_guide/mode_richards.rst
   /theory_guide/mode_th.rst
   /theory_guide/mode_general.rst
   /theory_guide/mode_hydrate.rst
   /theory_guide/mode_mphase.rst
   /theory_guide/mode_sco2.rst
   /theory_guide/constitutive_relations.rst

Reactive Transport Mode (Keyword CHEMISTRY)
+++++++++++++++++++++++++++++++++++++++++++

.. toctree::
   :maxdepth: 3

   /theory_guide/mode_reactive_transport.rst

Geophysics Modes
++++++++++++++++

.. toctree::
   :maxdepth: 3

   /theory_guide/mode_ert.rst

Well Models
++++++++++++++++

.. toctree::
   :maxdepth: 3

   /theory_guide/well.rst

Process Models Under Development
++++++++++++++++++++++++++++++++

.. toctree::
   :maxdepth: 2

   /theory_guide/multiple_continuum.rst
   /theory_guide/mode_geomechanics.rst

Numerical Methods
+++++++++++++++++

.. toctree::
   :maxdepth: 2

   /theory_guide/appendixB.rst
   /theory_guide/reaction_sandbox.rst

References
++++++++++

.. toctree::
   :maxdepth: 1

   /theory_guide/references.rst

.. The following are the hidden pages that are linked in the Theory Guide:
.. toctree::
   :hidden:

   /theory_guide/tg_intro.rst
   /theory_guide/mode_immis.rst
   /theory_guide/mode_miscible.rst
   /theory_guide/wipp_source_sink.rst
   /theory_guide/wipp_waste_form.rst
   /theory_guide/wipp_soln_controls.rst
   /theory_guide/pm_waste_form.rst
   /theory_guide/pm_ufd_decay.rst

